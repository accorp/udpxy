/* @(#) basic HTTP authentication.
 *
 *  Copyright 2008-2011 Pavel V. Cherenkov (pcherenkov@gmail.com)
 *
 *  This file is part of udpxy.
 *
 *  udpxy is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  udpxy is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with udpxy.  If not, see <http://www.gnu.org/licenses/>.
 */

/* strcasestr is a nonstandard extension */
#define _GNU_SOURCE

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>

#include "auth.h"

#define MAXLINE  256
#define MAXUSERS 16

static char **valid_users = NULL;
static size_t valid_users_count;  /* ..up to MAXUSERS */


/*
 *  base64 encoding routines are adopted from here:
 *     http://rosettacode.org/wiki/Base64_encode_data#Manual_implementation
 */

char *base64_encode(const char *data, size_t length)
{
    static const char alpha[] =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz"
        "0123456789+/";
    const long encoded_length = 4 * (length/3 + (length%3 ? 1 : 0)) + 1;
    char *encoded_data = malloc(encoded_length);
    char *encode = encoded_data;
    if( !encoded_data ) {
        (void) fprintf( stderr, "malloc failed for %ld bytes at: %s()\n",
            encoded_length, __func__ );
        return NULL;
    }
    while( length ) {
        uint32_t u =
            (uint32_t) (             data[0] << 16    )
          | (uint32_t) (length > 1 ? data[1] <<  8 : 0)
          | (uint32_t) (length > 2 ? data[2] <<  0 : 0);
        encode[0] =                     alpha[u >> 18     ];
        encode[1] =                     alpha[u >> 12 & 63];
        encode[2] = (length < 2 ? '=' : alpha[u >>  6 & 63]);
        encode[3] = (length < 3 ? '=' : alpha[u       & 63]);
        data += 3;
        encode += 4;
        length = ( length > 2 ? length - 3 : 0 );
    }
    encode[0] = '\0';
    return encoded_data;
}

int user_cmp(const char *user, const char *test, size_t test_len)
{
    while (1) {
        if (*user == '\0') {
            if (test_len == 0)
                return 0;
            return 1;
        }
        if (*user++ != *test++)
            return 2;
        test_len--;
    }
}


/* check presence of Basic HTTP Auth
 *
 * @param src          buffer with raw HTTP request
 * @param srclen       length of raw data
 *
 * @return 0 if authentication is successfully passed
 * @return 1 if request does not contain auth data
 * @return 2 if auth data has invalid format
 * @return 3 if username:password pair is wrong or unknown
 */
int
parse_auth( const char *src, size_t srclen)
{
    static const char PFX[] = "\r\nAuthorization: Basic ";
    const char *linestart;
    const char *datastart, *dataend;  /* ..data start, data end */
    size_t encoded_len;
    size_t n;

    if (!valid_users_count)
        return 0;

    linestart = strcasestr(src, PFX);
    if (!linestart)
        return 1;       /* ..request does not contain Basic HTTP Auth info */
    datastart = linestart + sizeof(PFX) - 1;
    if (datastart >= src + srclen)
        return 1;
    dataend = memchr(datastart, '\r', srclen - (size_t)(datastart - linestart));
    if (!dataend || dataend >= src + srclen)
        return 2;       /* ..auth data has invalid format */
    encoded_len = (size_t)(dataend - datastart);

    for (n = 0; n < valid_users_count; n++) {
        if (!user_cmp(valid_users[n], datastart, encoded_len))
            return 0;   /* ..found! */
    }
    return 3;           /* ..not found! */
}


/*  read given file and parse lines into valid_users array
 *
 *  @param fname name of text file containing username:userpass pairs (one per line)
 *
 *  @return  0 ok
 *  @return -1 cannot open file
 *  @return -2 cannot allocate memory
 */
int
read_authfile( const char *fname )
{
    char line[MAXLINE];
    FILE *f = fopen(fname, "r");
    if (!f)
        return -1;

    if (!valid_users)
        valid_users = malloc(MAXUSERS * sizeof(char*));
    if (!valid_users)
        return -2;
    valid_users_count = 0;

    while( fgets(line, sizeof(line), f) ) {
        char *user;
        char *begin = line;
        char *end = line + strlen(line) - 1;
        /* strip trailing CR/LF, spaces and tabs.. */
        while (begin <= end && isspace(*end))
            *end-- = '\0';
        /* skip beginning spaces and tabs.. */
        while (begin <= end && isspace(*begin))
            begin++;
        /* skip empty lines and comments.. */
        if (begin > end || *begin == '#')
            continue;
        user = base64_encode(begin, strlen(begin));
        if( !user ) {
            (void) fprintf( stderr, "Terminating process in order to prevent "
                                    "unauthorized access.\n" );
            exit(1);
        }
        valid_users[valid_users_count++] = user;
        if (valid_users_count == MAXUSERS)
            break;
    }
    fclose(f);
    return 0;
}

/* EOF */
